package ivs2017.test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import ivs2017.backend.*;
import ivs2017.profiling.Profiling;


public class test {

	Operator operator = new Operator();
	
	Memory memory = new Memory();
		
	double delta = 0.0;
	
	/** testy zakladnych operacii add,sub,mul,div,fact,power,root,mod **/
	@Test
	public void test_add() {
		
		assertEquals(7.0,operator.add(3.5, 3.5),delta);
	}
	@Test
	public void test_add2() {
		
		assertEquals(-9.0,operator.add(-5.0, -4.0),delta);
	}
	@Test
	public void test_sub(){
		
		assertEquals(5.5,operator.sub(10.0, 4.5),delta);
	}
	@Test
	public void test_sub2(){
		
		assertEquals(2.0,operator.sub(-2.0,-4.0),delta);
	}
	@Test
	public void test_mul(){
		
		assertEquals(19.24,operator.mul(3.7,5.2),delta);
	}
	@Test
	public void test_mul2(){
		
		assertEquals(31.85,operator.mul(-3.5,-9.1),delta);
	}
	@Test
	public void test_div(){
		
		assertEquals(5.0,operator.div(25.0,5.0),delta);
	}
	@Test
	public void test_div2(){
		
		assertEquals(3.1,operator.div(161.82,52.2),delta);
	}
	@Test
	public void test_fact(){
		
		assertEquals(1.0,operator.fact(0.0),delta);
	}
	@Test
	public void test_fact2(){
		
		assertEquals(1.0,operator.fact(1.0),delta);
	}
	@Test
	public void test_fact3(){
		
		assertEquals(720.0,operator.fact(6.0),delta);
	}
	@Test
	public void test_power(){
		
		assertEquals(4.0,operator.power(2.0,2.0),delta);
	}
	@Test
	public void test_power2(){
		
		assertEquals(0.008,operator.power(5.0,-3.0),delta);
	}
	@Test
	public void test_root(){
		
		assertEquals(3.0,operator.root(27.0, 3.0),delta);
	}
	@Test
	public void test_root2(){
		
		assertEquals(4.0,operator.root(256.0, 4.0),delta);
	}
	@Test
	public void test_mod(){
		
		assertEquals(2.3,operator.mod(5.4, 3.1),delta);
	}
	@Test
	public void test_mod2(){
		
		assertEquals(-1.7,operator.mod(-12.9, -5.6),delta);
	}
	
	/** testy prace s pamatou **/
	@Test
	public void test_clear_memory(){
		
		memory.MClear();
		assertEquals(0.0,memory.MRecall(),delta);
	}
	@Test
	public void test_store_recall(){
		
		memory.MClear();
		memory.MStore(8.6);
		assertEquals(8.6,memory.MRecall(),delta);
	}
	@Test
	public void test_add_memory(){
		
		memory.MClear();
		memory.MStore(5.1);
		memory.MAdd(4.9);
		assertEquals(10.0,memory.MRecall(),delta);
	}
	@Test
	public void test_sub_memory(){
		
		memory.MClear();
		memory.MStore(4.0);
		memory.MSub(10.0);
		assertEquals(-6.0,memory.MRecall(),delta);
	}
	
	/** testy pre smerodatnu odchylku */    
    double delta_deviation = 0.000000001; /** na 9 desatinnych miest zatial? */
    ArrayList<Double> numbers = new ArrayList<Double>();
    

	@Test
	public void test_deviation1(){
	    numbers.add(12.4);
	    numbers.add(45.78);
	    numbers.add(2.11);
	    numbers.add(4.01);
	    numbers.add(0.7856);
	    numbers.add(1.457);
	    numbers.add(-78.12);
	    numbers.add(8.112);
	    numbers.add(1.4);
	    numbers.add(-4.1234);
	    
	    assertEquals(30.6530982001, Profiling.Deviation(numbers),delta_deviation);
	}

	@Test
	public void test_deviation2(){
	    numbers.add(-12.4);
	    numbers.add(-45.78);
	    numbers.add(-2.11);
	    numbers.add(-4.01);
	    numbers.add(-0.7856);
	    numbers.add(-1.457);
	    numbers.add(-78.12);
	    numbers.add(-8.112);
	    numbers.add(-1.4);
	    numbers.add(-4.1234);
	    
	    assertEquals(25.7218374744, Profiling.Deviation(numbers),delta_deviation);
	}

	@Test
	public void test_deviation10(){
	    for (double i = 1.0; i <= 10.0; i += 1.0)
	    	numbers.add(i);
	    
	    assertEquals(3.027650354, Profiling.Deviation(numbers),delta_deviation);
	}
	@Test
	public void test_deviation100(){
	    for (double i = 1.0; i <= 100.0; i += 1.0)
	    	numbers.add(i);
	    
	    assertEquals(29.0114919759, Profiling.Deviation(numbers),delta_deviation);
	}
	
	@Test
	public void test_deviation1000(){
	    for (double i = 1.0; i <= 1000.0; i += 1.0)
	    	numbers.add(i);
	    
	    assertEquals(288.819436096, Profiling.Deviation(numbers),delta_deviation);
	}
}
