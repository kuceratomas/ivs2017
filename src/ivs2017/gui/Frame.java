/**
 * @file Frame.java
 * 
 * @brief Trieda Frame - trieda reprezentujuca hlavne okno GUI
 * @author Tomas Kucera xkucer90
 */

package ivs2017.gui;

import ivs2017.backend.ActionListenerFactory;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;

import java.awt.GridBagLayout;
import java.awt.event.KeyEvent;

/**
 * Trieda reprezentuje hlavne okno grafickeho uzivatelskeho rozhrania.
 * Obsahuje 2 displeje - primarny na vysledok a sekundarny na zobrazenie
 * uzivatelskeho vstupu. Pod displejmi su umiestnene tlacidla na pracu
 * s pamatou. Dalsou castou su tlacidla na matematicke operacie.
 * 
 * @brief Hlavne okno GUI
 */
public class Frame extends JFrame {

	private JPanel contentPane;
	private JTextField txtFieldSecondary;
	private JTextField txtFieldPrimary;
	private JPanel panel_1;
	private CalculatorButton btnRoot;
	private CalculatorButton btnDelete;
	private CalculatorButton btnClear;
	private CalculatorButton btnPower;
	private CalculatorButton btnFactorial;
	private CalculatorButton btnDivide;
	private CalculatorButton btnMultiply;
	private CalculatorButton btn7;
	private CalculatorButton btn8;
	private CalculatorButton btn9;
	private CalculatorButton btnMinus;
	private CalculatorButton btn4;
	private CalculatorButton btn5;
	private CalculatorButton btn6;
	private CalculatorButton btnPlus;
	private CalculatorButton btn1;
	private CalculatorButton btn2;
	private CalculatorButton btn3;
	private CalculatorButton btn0;
	private CalculatorButton btnDot;
	private CalculatorButton btnEquals;
	private CalculatorButton btnModulo;
	private JPanel panel_2;
	private MemoryButton btnMClear;
	private MemoryButton btnMPlus;
	private MemoryButton btnMMinus;
	private MemoryButton btnMRecall;
	private MemoryButton btnMStore;
	ActionListenerFactory listenerMaker;
	


	/**
	 * Hlavna metoda na spustenie aplikacie
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Frame frame = new Frame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Konstruktor triedy Frame.
	 */
	public Frame() {
		setTitle("Kalkulacka");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setMinimumSize(new Dimension(320,500));
		
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		/** panel obsahujuci 2 displeje */
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, -1)); // zaporna hodnota -> nie je medzi displejmi medzera
		
		/** sekundarny displej */
		txtFieldSecondary = new JTextField();
		panel.add(txtFieldSecondary, BorderLayout.NORTH);
		txtFieldSecondary.setColumns(10);
		txtFieldSecondary.setPreferredSize(new Dimension(txtFieldSecondary.getPreferredSize().width, 30));
		txtFieldSecondary.setBorder(new EmptyBorder(5, 5, 5, 5));
		txtFieldSecondary.setFocusable(false);
		txtFieldSecondary.setFont(new Font("Courier New", Font.PLAIN, 18));
		txtFieldSecondary.setHorizontalAlignment(SwingConstants.RIGHT);
		
		/** primarny displej */
		txtFieldPrimary = new JTextField();
		panel.add(txtFieldPrimary, BorderLayout.CENTER);
		txtFieldPrimary.setColumns(10);
		txtFieldPrimary.setPreferredSize(new Dimension(txtFieldPrimary.getPreferredSize().width, 70));
		txtFieldPrimary.setBorder(new EmptyBorder(5, 5, 5, 5));
		txtFieldPrimary.setFocusable(false);
		txtFieldPrimary.setFont(new Font("Courier New", Font.BOLD, 50));
		txtFieldPrimary.setHorizontalAlignment(SwingConstants.RIGHT);
		
		/** panel pre tlacidla na pracu s pamatou */
		panel_2 = new JPanel();
		panel.add(panel_2, BorderLayout.SOUTH);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel_2.setLayout(gbl_panel);
		panel_2.setBackground(new Color(232,232,232));

		/** panel pre tlacidla operacii a cisel */
		panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[]{0, 0};
		gbl_panel_1.rowHeights = new int[]{0, 0};
		gbl_panel_1.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panel_1.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel_1.setLayout(gbl_panel_1);
		panel_1.setBackground(Color.ORANGE);
		
		/** Tovaren na vyrobu prislusnych ActionListenerov */
		listenerMaker = new ActionListenerFactory(txtFieldSecondary, txtFieldPrimary);
		
		/** inicializacia tlacidiel na cisla a operacie */
		setUpCalculatorButtons();
		
		/** inicializacia tlacidiel na pamatove operacie */
		setUpMemoryButtons();
		
		/** Zaznamenavanie stlacenia tlacidla klavesnice */
		KeyboardFocusManager kbfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
		kbfm.addKeyEventDispatcher(new KDispatcher());
	  	
		pack();
	}
	
	/**
	 * Vytvorenie tlacidiel pre zadavanie operacii a operandov
	 *
	 */
	private void setUpCalculatorButtons() {
		
		btnRoot = new CalculatorButton("<html><sup>x</sup>\u221A</html>", 0, 0);
		btnRoot.addActionListener(listenerMaker.makeRootActionListener()); // odmocnina
		panel_1.add(btnRoot, btnRoot.getGbc());
		
		btnModulo = new CalculatorButton("%", 1, 0);
		btnModulo.addActionListener(listenerMaker.makeModuloActionListener());
		panel_1.add(btnModulo, btnModulo.getGbc());
		
		btnDelete = new CalculatorButton("DEL", 2, 0);
		btnDelete.addActionListener(listenerMaker.makeDeleteActionListener());
		panel_1.add(btnDelete, btnDelete.getGbc());
		
		btnClear = new CalculatorButton("C", 3, 0);
		btnClear.addActionListener(listenerMaker.makeClearActionListener());
		panel_1.add(btnClear, btnClear.getGbc());
		
		btnPower = new CalculatorButton("<html>x<sup>y</sup></html>", 0, 1);
		btnPower.addActionListener(listenerMaker.makePowerActionListener());
		panel_1.add(btnPower, btnPower.getGbc());
		
		btnFactorial = new CalculatorButton("x!", 1, 1);
		btnFactorial.addActionListener(listenerMaker.makeFactorialActionListener());
		panel_1.add(btnFactorial, btnFactorial.getGbc());
		
		btnDivide = new CalculatorButton("\u00F7", 2, 1);
		btnDivide.addActionListener(listenerMaker.makeDivideActionListener()); // delenie
		panel_1.add(btnDivide, btnDivide.getGbc());
		
		btnMultiply = new CalculatorButton("\u00D7", 3, 1);
		btnMultiply.addActionListener(listenerMaker.makeMultiplyActionListener()); // nasobenie
		panel_1.add(btnMultiply, btnMultiply.getGbc());
		
		btn7 = new CalculatorButton("7", 0, 2);
		btn7.addActionListener(listenerMaker.make7ActionListener());
		panel_1.add(btn7, btn7.getGbc());
		
		btn8 = new CalculatorButton("8", 1, 2);
		btn8.addActionListener(listenerMaker.make8ActionListener());
		panel_1.add(btn8, btn8.getGbc());
		
		btn9 = new CalculatorButton("9", 2, 2);
		btn9.addActionListener(listenerMaker.make9ActionListener());
		panel_1.add(btn9, btn9.getGbc());
		
		btnMinus = new CalculatorButton("-", 3, 2);
		btnMinus.addActionListener(listenerMaker.makeMinusActionListener());
		panel_1.add(btnMinus, btnMinus.getGbc());
		
		btn4 = new CalculatorButton("4", 0, 3);
		btn4.addActionListener(listenerMaker.make4ActionListener());
		panel_1.add(btn4, btn4.getGbc());
		
		btn5 = new CalculatorButton("5", 1, 3);
		btn5.addActionListener(listenerMaker.make5ActionListener());
		panel_1.add(btn5, btn5.getGbc());
		
		btn6 = new CalculatorButton("6", 2, 3);
		btn6.addActionListener(listenerMaker.make6ActionListener());
		panel_1.add(btn6, btn6.getGbc());
		
		btnPlus = new CalculatorButton("+", 3, 3);
		btnPlus.addActionListener(listenerMaker.makePlusActionListener());
		panel_1.add(btnPlus, btnPlus.getGbc());
		
		btn1 = new CalculatorButton("1", 0, 4);
		btn1.addActionListener(listenerMaker.make1ActionListener());
		panel_1.add(btn1, btn1.getGbc());
		
		btn2 = new CalculatorButton("2", 1, 4);
		btn2.addActionListener(listenerMaker.make2ActionListener());
		panel_1.add(btn2, btn2.getGbc());
		
		btn3 = new CalculatorButton("3", 2, 4);
		btn3.addActionListener(listenerMaker.make3ActionListener());
		panel_1.add(btn3, btn3.getGbc());
		
		btn0 = new CalculatorButton("0", 0, 5);
		btn0.addActionListener(listenerMaker.make0ActionListener());
		btn0.getGbc().gridwidth = 2;
		panel_1.add(btn0, btn0.getGbc());
		
		btnDot = new CalculatorButton(".", 2, 5);
		btnDot.addActionListener(listenerMaker.makeDotActionListener());
		panel_1.add(btnDot, btnDot.getGbc());
		
		btnEquals = new CalculatorButton("=", 3, 4);
		btnEquals.addActionListener(listenerMaker.makeEqualsActionListener());
		btnEquals.getGbc().gridheight = 2;
		panel_1.add(btnEquals, btnEquals.getGbc());
	}
	
	/**
	 * Vytvorenie tlacidiel pre pracu s pamatou
	 *
	 */
	private void setUpMemoryButtons() {
		btnMClear = new MemoryButton("MC", 0, 0);
		btnMClear.addActionListener(listenerMaker.makeMemoryClearActionListener());
		panel_2.add(btnMClear, btnMClear.getGbc());
		
		btnMPlus = new MemoryButton("M+", 1, 0);
		btnMPlus.addActionListener(listenerMaker.makeMemoryPlusActionListener());
		panel_2.add(btnMPlus, btnMPlus.getGbc());
		
		btnMMinus = new MemoryButton("M-", 2, 0);
		btnMMinus.addActionListener(listenerMaker.makeMemoryMinusActionListener());
		panel_2.add(btnMMinus, btnMMinus.getGbc());
		
		btnMRecall = new MemoryButton("MR", 3, 0);
		btnMRecall.addActionListener(listenerMaker.makeMemoryRecallActionListener());
		panel_2.add(btnMRecall, btnMRecall.getGbc());
		
		btnMStore = new MemoryButton("MS", 4, 0);
		btnMStore.addActionListener(listenerMaker.makeMemoryStoreActionListener());
		panel_2.add(btnMStore, btnMStore.getGbc());
	}
	
	/**
	 * Trieda reagujuca na stlacenie tlacidla na klavesnici. Pri validnom vstupe
	 * je vykonana akcia prisluchajuca prislusnemu tlacidlu, inak je akcia ignorovana
	 * 
	 * @brief Reakcia na stlacenie tlacidla na klavesnici
	 */
	private class KDispatcher implements KeyEventDispatcher {
        @Override
        public boolean dispatchKeyEvent(KeyEvent e) {
            if (e.getID() == KeyEvent.KEY_PRESSED) {
            	
            	switch (e.getKeyChar()) {
            		
	            	case '0':
	            		btn0.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '1':
	            		btn1.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '2':
	            		btn2.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '3':
	            		btn3.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '4':
	            		btn4.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '5':
	            		btn5.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '6':
	            		btn6.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '7':
	            		btn7.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '8':
	            		btn8.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '9':
	            		btn9.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '.':
	            	case ',':
	            		btnDot.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case KeyEvent.VK_DELETE:
	            	case KeyEvent.VK_BACK_SPACE:
	            		btnDelete.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case KeyEvent.VK_C:
	            		btnClear.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '*':
	            		btnMultiply.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '/':
	            		btnDivide.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case KeyEvent.VK_F:
	            	case '!':
	            		btnFactorial.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '+':	
	            		btnPlus.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '-':
	            		btnMinus.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case KeyEvent.VK_E:
	            	case KeyEvent.VK_P:
	            		btnPower.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case KeyEvent.VK_R:
	            		btnRoot.getActionListeners()[0].actionPerformed(null);
	            		break;
	            		
	            	case '%':
	            		btnModulo.getActionListeners()[0].actionPerformed(null);
	            		break;

	            		
	            	case '=':
	            	case KeyEvent.VK_ENTER:
	            		btnEquals.getActionListeners()[0].actionPerformed(null);
	            		break;

	            		
	            	default:
	            		break;
 
            	}
            	
            	
            	switch (e.getKeyCode()) {
	            	case KeyEvent.VK_LEFT:
	            		
	            		if (txtFieldPrimary.getCaretPosition() > 0)
	            			txtFieldPrimary.setCaretPosition(txtFieldPrimary.getCaretPosition() - 1);
	            		
	            		if (txtFieldSecondary.getCaretPosition() > 0)
	            			txtFieldSecondary.setCaretPosition(txtFieldSecondary.getCaretPosition() - 1);
	            		
	            		break;
	            		
	            	case KeyEvent.VK_RIGHT:
	            		
	            		if (txtFieldPrimary.getCaretPosition()+1 < txtFieldPrimary.getText().length())
	            			txtFieldPrimary.setCaretPosition(txtFieldPrimary.getCaretPosition() + 1);
	            		
	            		if (txtFieldSecondary.getCaretPosition()+1 < txtFieldSecondary.getText().length())
	            			txtFieldSecondary.setCaretPosition(txtFieldSecondary.getCaretPosition() + 1);
	            		
	            		break;
            	
            	}

            } // stlacenie tlacidla klavesnice
            
            return false;
        }
    }

}
