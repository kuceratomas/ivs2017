/**
 * @file CalculatorButton.java
 * 
 * @brief Trieda CalculatorButton - trieda reprezentujuca matematicke tlacidlo
 * @author Tomas Kucera xkucer90
 */

package ivs2017.gui;

import java.awt.Dimension;
import java.awt.Insets;

/**
 * Trieda, ktorej objekty su umiestnene v hlavnej casti
 * aplikacie a sluzia na vykonavanie matematickych operacii.
 * Rozsiruje triedu ivs2017.gui.MemoryButton
 * 
 * @brief Pamatove tlacidla
 */
public class CalculatorButton extends MemoryButton{
	
	/**
	   * Konstruktor triedy CalculatorButton
	   *
	   * @param name Nazov tlacidla
	   * @param x riadok
	   * @return y stlpec
	   */
	public CalculatorButton(String name, int x, int y) {
		super(name, x, y);
		
		setPreferredSize(new Dimension(60, 50));
		getGbc().insets = new Insets(3,3,3,3);
		
	}

}
