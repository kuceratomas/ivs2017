/**
 * @file MemoryButton.java
 * 
 * @brief Trieda MemoryButton - trieda reprezentujuca pamatove tlacidlo
 * @author Tomas Kucera xkucer90
 */

package ivs2017.gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.border.EmptyBorder;
/**
 * Trieda, ktorej objekty su umiestnene pod hlavnym displejom
 * aplikacie a sluzia na pracu s pamatou (pridanie, vymazanie a nacitanie).
 * Rozsiruje triedu javax.swing.JButton
 * 
 * @brief Pamatove tlacidla
 */
public class MemoryButton extends JButton{
	
	private GridBagConstraints gbc;
	JButton button;
	
	  /**
	   * Konstruktor triedy MemoryButton
	   *
	   * @param name Nazov tlacidla
	   * @param x riadok
	   * @return y stlpec
	   */
	public MemoryButton(String name, int x, int y) {
		super(name);
		
		button = this;
		
		setBorder(new EmptyBorder(0, 0, 0, 0));
		setRolloverEnabled(false);
		setBackground(Color.WHITE);
		setFocusPainted(false);
		setCursor(new Cursor(Cursor.HAND_CURSOR));
		setFont(new Font("Courier New", Font.PLAIN, 20));
		
		
		gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 1;
		gbc.weighty = 1;
		gbc.insets = new Insets(0,1,15,1);
		gbc.gridx = x;
		gbc.gridy = y;
		
		/** pridanie MouseListeneru na zmenu farby pri najdeni mysou na objekt */
		addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		        button.setBackground(new Color(232,232,232));
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		    	button.setBackground(Color.WHITE);
		    }
		});
		
		
	}
	
	/**
	 * Metoda vrati privatnu premennu gbc typu GridBagConstraints pre pozicovanie objektu
	 * @return Vrati GBC pre pozicovanie objektu
	 */
	public GridBagConstraints getGbc() {
		return this.gbc;
	}
	
	

}
