/** 
 * Trieda Profiling - trieda pre profiling so smerodatnou odchylkou
 * @author Samuel Jakubík <xjakub31@stud.fit.vutbr.cz>
 */

package ivs2017.profiling;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

import ivs2017.backend.Operator;

/**
 * @class Trieda pre funkciu smerodatnej odchylky 
 * Pre profiling samostatne mimo @ivs2017.backend.Operator
 * @author Samuel Jakubik
 *	
 */
public class Profiling {

	public static void main(String[] args) {
	    
	    ArrayList<Double> numbers = new ArrayList<Double>();  /** Pole zadanych hodnot */
	    
	    /** Vsekty ciselne hodnoty pokial nepride na hodnotu neciselnu */
	    if (args[0].equals("-i") || args[0].equals("--input")){
		    if(args.length <= 1){
	    		System.out.println("No valid numbers (int or double values) were given!");
	    		System.exit(1);
		    }
	    	
	    	for (int i = 1; i < args.length; i++){
	    		try{
	    			numbers.add(Double.valueOf(args[i]));
	    		}
	    		catch(NumberFormatException exception){
	    		    if(i == 1){
	    	    		System.out.println("No valid numbers (int or double values) were given!");
	    	    		System.exit(1);
	    		    }
	    			break;
	    		}	    		
	    	}
	    	
	    	System.out.println("Smerodatna odchylka: "+Deviation(numbers));
	    }
	    else{
		    Scanner in = new Scanner(System.in);  /** Scanner na spracovanie vstupu */
		    System.out.print("Zadajte cisla: ");
		    while(in.hasNextDouble()){  
		    	try{
		    		double number = in.nextDouble();	    	
		    		numbers.add(number);
		    	}
		    	catch(NoSuchElementException exception){
		    		break;
		    	}
		    }

		    /** Pokial prva hodnota bola neciselna */
		    if(numbers.isEmpty()){
	    		System.out.println("No valid numbers (int or double values) were given!");
	    		System.exit(1);
			    in.close();
		    }
		    		    
		    System.out.println("Smerodatna odchylka: "+Deviation(numbers));
		    	    
		    in.close();
	    }
	}
	
	/**
	 * Konstruktor triedy Profiling
	 */
	public Profiling(){
		
	}
	
	/**
	 * Vypocita smerodatnu odchylku na zaklade zadanych hodnot. Presnost na +/- 9 desatinnych miest.
	 * @param numbers -- pole s hodnotami z ktorych bude vypocitana smerodatna odchylka
	 * @return Smerodatna odchylka
	 */
	public static double Deviation(ArrayList<Double> numbers){
		Operator op = new Operator();
		
		double result = 0.0;  /** Premenna drziaca vysledok vypoctu */
		
		/** Vypocitaj priemer (x') */
		result = op.div(op.add(result, Sum(numbers)), numbers.size());
		
		/** Umocni a vynasob poctom (N*(x')^2) */
		result = op.mul(op.power(result, 2), numbers.size());
		
		double tmp_result = 0.0; /** Pomocna premenna na prechodne drzanie vysledku */
		
		/** Vypocitaj sumu druhych mocnin cisel (SUM[i=1 -> N] (xi^2) */
		
		for (int i = 0; i < numbers.size(); i++){
			tmp_result = op.add(tmp_result, op.power(numbers.get(i), 2));
		}		
		
		/** Odpocitaj vynasobeny priemer od sumy */
		result = op.sub(tmp_result, result);
		
		/** Vynasob 1/(N-1) */		
		result = op.mul(op.div(1, op.sub(numbers.size(), 1)), result);
		
		/** Odmocni */
		result = op.root(result, 2);
		
		return result;
	}
	
	/**
	 * Funkcia na spocitanie hodnot ArrayList-u
	 * Takova mensia SUMa
	 * @param numbers -- pole hodnot na spocitanie
	 * @return suma hodnot zo zadaneho pola
	 */
	private static double Sum(ArrayList<Double> numbers){
		Operator op = new Operator();
		double sum = 0.0;
		for(int i = 0; i < numbers.size(); i++){
			sum = op.add(sum, numbers.get(i));
		}		
		return sum;
	}
}	// Profiling 
/*** Koniec suboru Profiling.java ***/
