/**
 * @file Pair.java
 * 
 * @brief Trieda Pair - Abstraktna datova struktura Pair
 * @author Tomas Kucera xkucer90
 */

package ivs2017.backend;

/**
 * Trieda reprezentujuca ADS Pair. Sluzi na vytvorenie paru
 * hodnota tokenu - lexema tokenu.
 * 
 * @brief ADS Pair
 */
public class Pair<K, V> {

    private final K hodnota;
    private final V lexema;

    /**
     * Staticka metoda pre vytvaranie objektov typu Pair
     * @param hodnota hodnota tokenu
     * @param lexema lexema tokenu
     * @return Vrati objekt typu Pair
     */
    public static <K, V> Pair<K, V> createPair(K hodnota, V lexema) {
        return new Pair<K, V>(hodnota, lexema);
    }

    /**
     * Konstruktor triedy Pair
     * @param hodnota hodnota tokenu
     * @param lexema lexema tokenu
     */
    public Pair(K hodnota, V lexema) {
        this.hodnota = hodnota;
        this.lexema = lexema;
    }

    /**
     * metoda vrati prvy prvok paru - hodnota
     * @return Vrati prvy prvok paru (hodnotu)
     */
    public K first() {
        return hodnota;
    }

    /**
     * metoda vrati druhy prvok paru - lexema
     * @return Vrati druhy prvok paru (lexemu)
     */
    public V second() {
        return lexema;
    }

}