/** 
 * Trieda Operator - trieda s operaciami pre matematicku kniznicu
 * @author Samuel Jakub�k <xjakub31@stud.fit.vutbr.cz>
 */

package ivs2017.backend;

import java.math.BigDecimal;

/**
 * @class Trieda zahrnujuca vsetky podporovane matematicke operacie: 
 * scitanie (+), odcitanie (-), nasobenie (*), delenie (/), mocnina, odmocnina, faktorial, modulo
 */
public class Operator {
	
	public Operator() {
	}

	/**
	 * Scita dve cisla dane parametrami funkcie.
	 * @param x -- prve cele alebo realne cislo ku ktoremu pricita
	 * @param y -- druhe cele alebo realne cislo ktore pricita
	 * @return Sucet dvoch zadanych cisel [x + y].
	 */
	public double add(double x, double y){
		return round(x + y);
	}

	/**
	 * Odcita dve cisla dane parametrami funkcie.
	 * @param x -- prve cele alebo realne cislo od ktoreho odcita
	 * @param y -- druhe cele alebo realne cislo ktore odcita
	 * @return Rozdiel dvoch zadanych cisel [x - y].
	 */
	public double sub(double x, double y){
		return round(x - y);
	}

	/**
	 * Vynasobi dve cisla dane parametrami funkcie.
	 * @param x -- prve cele alebo realne cislo ktore je nasobencom
	 * @param y -- druhe cele alebo realne cislo ktore je nasobitelom
	 * @return Vysledok nasobenia dvoch cisel [x * y].
	 */
	public double mul(double x, double y){
		return round(x * y);
	}

	/**
	 * Vydeli dve cisla dane parametrami funkcie.
	 * @param x -- prve cele alebo realne cislo ktore je delencom
	 * @param y -- druhe cele alebo realne cislo ktore je delitelom
	 * @throws IllegalArgumentException pri deleni nulou
	 * @return Podiel dvoch cisel [x / y].
	 */
	public double div(double x, double y){
		if (y == 0)
			throw new IllegalArgumentException();
		else			
			return round(x / y);
	}

	/**
	 * Faktorial zadaneho cisla.
	 * @param x -- cele alebo realne cislo
	 * @throws IllegalArgumentException pri zapornom faktoriale
	 * @return Faktorial zadaneho cisla (!x).
	 */
	public double fact(double x){
		if (x < 0){
			double tmp = round(-x * fact(-x - 1));
			return -tmp;
		}
		else if (x >= 0 && x <= 1)
			return 1;
		else{
			return round(x * fact(x - 1));
		}
	}	

	/**
	 * Umocni cislo dane prvym parametrom na cislo dane druhym parametrom.
	 * @param x -- prve cele alebo realne cislo ktore je zakladom mocniny
	 * @param y -- druhe cele alebo realne cislo ktore je exponentom
	 * @return Y-tu (druhy parameter) mocninu cisla X (prvy parameter) [x^y].
	 */
	public double power(double x, double y){
		return round(Math.pow(x, y));
	}

	/**
	 * Odmocnina cisla daneho prvym parametrom, radu daneho druhym parametrom.
	 * @param x -- prve cele alebo realne cislo ktore je odmocnovane
	 * @param y -- druhe cele alebo realne cislo ktore urcuje rad odmocniny
	 * @throws IllegalArgumentException pri odmocnine zo zaporneho cisla
	 * @return Vysledok Y-tej (druhy parameter) odmocniny cisla X (prvy parameter).
	 */
	public double root(double x, double y){
		if (x < 0)
			throw new IllegalArgumentException();			
		return round(Math.pow(x, 1/y));
	}

	/**
	 * Zbytok po deleni. Cisla nemusia byt celociselne, su povolene aj realne cisla.
	 * @param x -- prve cele alebo realne cislo ktore je delencom
	 * @param y -- druhe cele alebo realne cislo ktore je delitelom
	 * @throws IllegalArgumentException pri deleni nulou
	 * @return Zvysok po deleni prveho cisla (x) druhym (y) [x % y].
	 */
	public double mod(double x, double y){
		if (y == 0)
			throw new IllegalArgumentException();
		return round(x % y);
	}
	
	private double round(double x){
		return new BigDecimal(String.valueOf(x)).setScale(12, BigDecimal.ROUND_HALF_UP).doubleValue();
	}	
}	// Operator 
/*** Koniec suboru Operator.java ***/
