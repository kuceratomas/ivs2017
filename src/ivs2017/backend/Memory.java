/**
 * @file Memory.java
 * 
 * Trieda Memory - trieda pre funkcie ukladania a nacitavania cisel/medzivypoctov do/z pamate
 * @author Samuel Jakubík <xjakub31@stud.fit.vutbr.cz>
 */

package ivs2017.backend;

/**
 * @class Trieda zahrnujuca funkcie na pracu s ukladanim a nacitavanim cisel do/z pamate:
 * MC (Memory Clear), MR (Memory Recall), MS (Memory Store), M+ (Memory Add), M- (Memory Subtract)
 */
public class Memory {	
	private double memory;	//pamat kalkulacky udrzujuca cisla
	
	public Memory(){
		memory = 0.0;		
	}
	
	/**
	 * Vymaze pamat kalkulacky
	 */
	public void MClear(){
		memory = 0.0;
	}
	
	/**
	 * Vrati obsah pamate kalkulacky
	 * @return cislo ulozene v pamati
	 */
	public double MRecall(){
		return memory;
	}
	
	/**
	 * Ulozi zadane cislo do pamate
	 * @param x -- cislo ktore ma byt ulozene do pamate
	 */
	public void MStore(double x){
		memory = x;
	}
	
	/**
	 * K zadanemu cislu pripocita obsah pamate a vysledok ulozi do pamate
	 * @param x -- cislo ku ktoremu bude pripocitany obsah pamate
	 */
	public void MAdd(double x){
		memory += x;
	}

	/**
	 * Od zadaneho cisla odpocita obsah pamate a vysledok ulozi do pamate
	 * @param x -- cislo od ktoreho bude odpocitany obsah pamate
	 */
	public void MSub(double x){
		memory -= x;
	}
	
}	// Memory 
/*** Koniec suboru Memory.java ***/
