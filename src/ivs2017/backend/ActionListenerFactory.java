/**
 * @file ActionListenerFactory.java
 * 
 * @brief Trieda ActionListenerFactory - trieda vyrabajuca objekty reagujuce na uzivatelsky vstup
 * @author Tomas Kucera xkucer90
 */

package ivs2017.backend;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import java.util.Stack;

import javax.swing.JTextField;




/**
 * Trieda, ktora na ziadost vyrobi pozadovany objekt implementujuci
 * rozhranie java.awt.event.ActionListener. Tieto objekty spracuju vstup
 * z klavesnice alebo tlacidla GUI a nasledne vykonaju pozadovanu akciu.
 * 
 * @brief Vyroba objektov reagujucich na uzivatelsky vstup
 */
public class ActionListenerFactory {
	
	/** objekt pre pracu s pamatou kalkulacky **/
	Memory memory = new Memory();
	
	/** maximalne jedna desatina ciarka **/
	private boolean tmp = true;
	
	/** zaporne cisla **/
	private boolean op_minus = true;
	
	/** prve cislo zaporne **/
	private boolean first_number = false;
	
	/** displej s uzivatelskym vstupom */
	private JTextField pane1;
	
	/** hlavny displej zobrazujuci vysledok */
	private JTextField pane2;
	
	/** zasobnik tokenov zadanych uzivatelom */
	private List<Pair<TokenType, String>> symbols;
	
	/** enum reprezentujuci typ tokenov */
	private enum TokenType {
		NUMBER(0),
		OPERATOR(1),
		DOT(2),
		FACTORIAL(3),
		TOP(4),
		MINUS(5);
		
		private int value; // hodnota tokenu
		
		/**
		 * Konstruktor pre objekt reprezentujuci typ tokenu
		 *
		 * @param value hodnota typu tokenu
		 */
		private TokenType(int value) {
			this.value = value;
		}
	}
	
	/** enum znak precedencnej tabulky */
	private enum PT {
		SH(0),
		RE(1),
		HA(2),
		ER(3);
		
		private int value; // hodnota tokenu
		
		/**
		 * Konstruktor pre objekt reprezentujuci znak precedencnej tabulky
		 *
		 * @param value znak precedencnej tabulky
		 */
		private PT(int value) {
			this.value = value;
		}
	}
	
	PT PTable[][] = {

	//					 + -	/*     ^^-     !  	  (      )      $
	/* 		+ - 	*/{ PT.RE, PT.SH, PT.SH, PT.SH, PT.SH, PT.RE, PT.RE },
	/*		/ * 	*/{ PT.RE, PT.RE, PT.SH, PT.SH, PT.SH, PT.RE, PT.RE},
	/*		^ ^- 	*/{ PT.RE, PT.RE, PT.SH, PT.SH, PT.SH, PT.RE, PT.RE},
	/* 		! 		*/{ PT.RE, PT.RE, PT.RE, PT.ER, PT.ER, PT.RE, PT.RE},
	/* 		( 		*/{ PT.SH, PT.SH, PT.SH, PT.SH, PT.SH, PT.HA, PT.ER},
	/*		) 		*/{ PT.RE, PT.RE, PT.RE, PT.RE, PT.ER, PT.RE, PT.RE},
	/* 		$ 		*/{ PT.SH, PT.SH, PT.SH, PT.SH, PT.SH, PT.ER, PT.ER}
	};
	
	/**
	 * Konstruktor pre objekt tovarne vyrabajucej objekty reagujuce
	 * na uzivatelsky vstup
	 *
	 * @param pane1 displej s uzivatelskym vstupom
	 * @param pane2 hlavny displej zobrazujuci vysledok
	 */
	public ActionListenerFactory(JTextField pane1, JTextField pane2) {
		this.pane1 = pane1;
		this.pane2 = pane2;
		symbols = new ArrayList<Pair<TokenType, String>>();
		symbols.add(Pair.createPair(TokenType.TOP, "$"));
	}
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku 'MC'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku 'MC'
	 */
	public ActionListener makeMemoryClearActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				memory.MClear();
			}
		};
	}
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku 'M+'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku 'M+'
	 */
	public ActionListener makeMemoryPlusActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (pane2.getText() != null){
					memory.MAdd(Double.parseDouble(pane2.getText()));
				}
			}
		};
	}
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku 'M-'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku 'M-'
	 */
	public ActionListener makeMemoryMinusActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (pane2.getText() != null){
					memory.MSub(Double.parseDouble(pane2.getText()));
				}
			}
		};
	}
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku 'MR'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku 'MR'
	 */
	public ActionListener makeMemoryRecallActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(memory.MRecall());
				if (symbols.get(symbols.size()-1).first() != TokenType.FACTORIAL) {
					if (first_number)
						symbols.add(Pair.createPair(TokenType.NUMBER, Double.toString(-memory.MRecall())));
					else
						symbols.add(Pair.createPair(TokenType.NUMBER, Double.toString(memory.MRecall())));
					if (!tmp) {pane2.setText("");}
					tmp = true;
					op_minus = true;
					double res = memory.MRecall();
					int res_int = (int) Math.round(res);
					String result = Double.toString(memory.MRecall());
					if (res == res_int){
						result = Integer.toString(res_int);
					}
					pane1.setText(pane1.getText() + result);
					if (first_number){
						pane2.setText(pane2.getText() + "-" + result);
						first_number = false;
					}
					else
						pane2.setText(pane2.getText() + result);
					replaceToken();
				}
			}
		};
	}
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku 'MS'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku 'MS'
	 */
	public ActionListener makeMemoryStoreActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (pane2.getText() != null){
					memory.MStore(Double.parseDouble(pane2.getText()));
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '0'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '0'
	 */
	public ActionListener make0ActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// osetrit viac nul za sebou? !((symbols.get(symbols.size()-1)).second().equals("0")
				if (symbols.get(symbols.size()-1).first() != TokenType.FACTORIAL) {
					if (first_number){
						symbols.add(Pair.createPair(TokenType.NUMBER, "0"));
						first_number = false;
					}
					else
						symbols.add(Pair.createPair(TokenType.NUMBER, "0"));
					if (!tmp) {pane2.setText("");}
					tmp = true;
					op_minus = true;
					pane1.setText(pane1.getText() + "0");
					pane2.setText(pane2.getText() + "0");
					replaceToken();
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '1'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '1'
	 */
	public ActionListener make1ActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() != TokenType.FACTORIAL) {
					if (first_number)
						symbols.add(Pair.createPair(TokenType.NUMBER, "-1"));
					else
						symbols.add(Pair.createPair(TokenType.NUMBER, "1"));
					if (!tmp) {pane2.setText("");}
					tmp = true;
					op_minus = true;
					pane1.setText(pane1.getText() + "1");
					if (first_number){
						pane2.setText(pane2.getText() + "-1");
						first_number = false;
					}
					else
						pane2.setText(pane2.getText() + "1");
					replaceToken();
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '2'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '2'
	 */
	public ActionListener make2ActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() != TokenType.FACTORIAL) {
					if (first_number)
						symbols.add(Pair.createPair(TokenType.NUMBER, "-2"));
					else
						symbols.add(Pair.createPair(TokenType.NUMBER, "2"));
					if (!tmp) {pane2.setText("");}
					tmp = true;
					op_minus = true;
					pane1.setText(pane1.getText() + "2");
					if (first_number){
						pane2.setText(pane2.getText() + "-2");
						first_number = false;
					}
					else
						pane2.setText(pane2.getText() + "2");
					replaceToken();
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '3'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '3'
	 */
	public ActionListener make3ActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() != TokenType.FACTORIAL) {
					if (first_number)
						symbols.add(Pair.createPair(TokenType.NUMBER, "-3"));
					else
						symbols.add(Pair.createPair(TokenType.NUMBER, "3"));
					if (!tmp) {pane2.setText("");}
					tmp = true;
					op_minus = true;
					pane1.setText(pane1.getText() + "3");
					if (first_number){
						pane2.setText(pane2.getText() + "-3");
						first_number = false;
					}
					else
						pane2.setText(pane2.getText() + "3");
					replaceToken();
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '4'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '4'
	 */
	public ActionListener make4ActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() != TokenType.FACTORIAL) {
					if (first_number)
						symbols.add(Pair.createPair(TokenType.NUMBER, "-4"));
					else
						symbols.add(Pair.createPair(TokenType.NUMBER, "4"));
					if (!tmp) {pane2.setText("");}
					tmp = true;
					op_minus = true;
					pane1.setText(pane1.getText() + "4");
					if (first_number){
						pane2.setText(pane2.getText() + "-4");
						first_number = false;
					}
					else
						pane2.setText(pane2.getText() + "4");
					replaceToken();
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '5'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '5'
	 */
	public ActionListener make5ActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() != TokenType.FACTORIAL) {
					if (first_number)
						symbols.add(Pair.createPair(TokenType.NUMBER, "-5"));
					else
						symbols.add(Pair.createPair(TokenType.NUMBER, "5"));
					if (!tmp) {pane2.setText("");}
					tmp = true;
					op_minus = true;
					pane1.setText(pane1.getText() + "5");
					if (first_number){
						pane2.setText(pane2.getText() + "-5");
						first_number = false;
					}
					else
						pane2.setText(pane2.getText() + "5");
					replaceToken();
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '6'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '6'
	 */
	public ActionListener make6ActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() != TokenType.FACTORIAL) {
					if (first_number)
						symbols.add(Pair.createPair(TokenType.NUMBER, "-6"));
					else
						symbols.add(Pair.createPair(TokenType.NUMBER, "6"));
					if (!tmp) {pane2.setText("");}
					tmp = true;
					op_minus = true;
					pane1.setText(pane1.getText() + "6");
					if (first_number){
						pane2.setText(pane2.getText() + "-6");
						first_number = false;
					}
					else
						pane2.setText(pane2.getText() + "6");
					replaceToken();
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '7'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '7'
	 */
	public ActionListener make7ActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() != TokenType.FACTORIAL) {
					if (first_number)
						symbols.add(Pair.createPair(TokenType.NUMBER, "-7"));
					else
						symbols.add(Pair.createPair(TokenType.NUMBER, "7"));
					if (!tmp) {pane2.setText("");}
					tmp = true;
					op_minus = true;
					pane1.setText(pane1.getText() + "7");
					if (first_number){
						pane2.setText(pane2.getText() + "-7");
						first_number = false;
					}
					else
						pane2.setText(pane2.getText() + "7");
					replaceToken();
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '8'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '8'
	 */
	public ActionListener make8ActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() != TokenType.FACTORIAL) {
					if (first_number)
						symbols.add(Pair.createPair(TokenType.NUMBER, "-8"));
					else
						symbols.add(Pair.createPair(TokenType.NUMBER, "8"));
					if (!tmp) {pane2.setText("");}
					tmp = true;
					op_minus = true;
					pane1.setText(pane1.getText() + "8");
					if (first_number){
						pane2.setText(pane2.getText() + "-8");
						first_number = false;
					}
					else
						pane2.setText(pane2.getText() + "8");
					replaceToken();
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '9'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '9'
	 */
	public ActionListener make9ActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() != TokenType.FACTORIAL) {
					if (first_number)
						symbols.add(Pair.createPair(TokenType.NUMBER, "-9"));
					else
						symbols.add(Pair.createPair(TokenType.NUMBER, "9"));
					if (!tmp) {pane2.setText("");}
					tmp = true;
					op_minus = true;
					pane1.setText(pane1.getText() + "9");
					if (first_number){
						pane2.setText(pane2.getText() + "-9");
						first_number = false;
					}
					else
						pane2.setText(pane2.getText() + "9");
					replaceToken();
				}
			}
		};
	}
	
	/**
	 * Metoda pre vytvorenie jedneho tokenu pre viacero zadanych cislic.
	 * Spocita pocet tokenov iducich za sebou a reprezentujucich jedno cislo
	 * a vytvori jeden jednotny token, nasledne tokeny vyberie zo zasobniku
	 * a vlozi nan novo vytvoreny token.
	 */
	private void replaceToken() {
		int counter = 1;
		while (symbols.get(symbols.size() - counter).first() == TokenType.NUMBER || symbols.get(symbols.size() - counter).first() == TokenType.DOT || symbols.get(symbols.size() - counter).first() == TokenType.MINUS) {
			counter++;
		}
		counter--;
		String number = "";
		for (int i = 0; i < counter; i++) {
			number = symbols.get(symbols.size()-1).second() + number;
			symbols.remove(symbols.get(symbols.size()-1));
		}
		symbols.add(Pair.createPair(TokenType.NUMBER, number)); // nahradenie jednotlivych tokenov jednotnym tokenom
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '+'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '+'
	 */
	public ActionListener makePlusActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() == TokenType.NUMBER || symbols.get(symbols.size()-1).first() == TokenType.FACTORIAL) {
					//Double.parseDouble(pane1.getText())
					tmp = false;
					symbols.add(Pair.createPair(TokenType.OPERATOR, "+"));
					pane1.setText(pane1.getText() + "+");
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '-'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '-'
	 */
	public ActionListener makeMinusActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if ((symbols.get(symbols.size()-1).first() == TokenType.OPERATOR) && (op_minus == true)){
					op_minus = false;
					symbols.add(Pair.createPair(TokenType.MINUS, "-"));
					tmp = false;
					pane1.setText(pane1.getText() + "-");
				}
				else if (symbols.get(symbols.size()-1).first() == TokenType.NUMBER || symbols.get(symbols.size()-1).first() == TokenType.FACTORIAL || symbols.get(symbols.size()-1).first() == TokenType.TOP ) {
					if ((first_number == false) && (symbols.get(symbols.size()-1).first() == TokenType.TOP)) {
						first_number = true;
					}
					else{
						symbols.add(Pair.createPair(TokenType.OPERATOR, "-"));
					}
					tmp = false;
					pane1.setText(pane1.getText() + "-");
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '*'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '*'
	 */
	public ActionListener makeMultiplyActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() == TokenType.NUMBER || symbols.get(symbols.size()-1).first() == TokenType.FACTORIAL) {
					symbols.add(Pair.createPair(TokenType.OPERATOR, "*"));
					tmp = false;
					pane1.setText(pane1.getText() + "*");
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '/'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '/'
	 */
	public ActionListener makeDivideActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() == TokenType.NUMBER || symbols.get(symbols.size()-1).first() == TokenType.FACTORIAL) {
					symbols.add(Pair.createPair(TokenType.OPERATOR, "/"));
					tmp = false;
					pane1.setText(pane1.getText() + "/");
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '%'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '%'
	 */
	public ActionListener makeModuloActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() == TokenType.NUMBER || symbols.get(symbols.size()-1).first() == TokenType.FACTORIAL) {
					symbols.add(Pair.createPair(TokenType.OPERATOR, "%"));
					tmp = false;
					pane1.setText(pane1.getText() + "%");
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '!'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '!'
	 */
	public ActionListener makeFactorialActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() == TokenType.NUMBER) {
					symbols.add(Pair.createPair(TokenType.FACTORIAL, "!"));
					tmp = false;
					pane1.setText(pane1.getText() + "!");
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '^'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '^'
	 */
	public ActionListener makePowerActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() == TokenType.NUMBER || symbols.get(symbols.size()-1).first() == TokenType.FACTORIAL) {
					symbols.add(Pair.createPair(TokenType.OPERATOR, "^"));
					tmp = false;
					pane1.setText(pane1.getText() + "^");
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku "\u221A" (odmocnina)
	 *
	 * @return Vrati objekt reagujuci na vstup znaku "\u221A" (odmocnina)
	 */
	public ActionListener makeRootActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() == TokenType.NUMBER || symbols.get(symbols.size()-1).first() == TokenType.FACTORIAL) {
					symbols.add(Pair.createPair(TokenType.OPERATOR, "\u221A"));
					tmp = false;
					pane1.setText(pane1.getText() + "\u221A");
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku 'C' (clear)
	 *
	 * @return Vrati objekt reagujuci na vstup znaku 'C' (clear)
	 */
	public ActionListener makeClearActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				symbols.clear();
				symbols.add(Pair.createPair(TokenType.TOP, "$"));
				pane1.setText("");
				pane2.setText("");
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku 'Delete/Backspace' (vymazanie posledneho znaku)
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '^' (vymazanie posledneho znaku)
	 */
	public ActionListener makeDeleteActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.size() > 1 && pane1.getText() != null) {
					 // ak je co odstranovat
					if (symbols.get(symbols.size()-1).first() == TokenType.NUMBER) {
						String panel = pane1.getText();
						if (pane1.getText() == null)
							first_number = false;
						// posledne je cislo, je potreba odstranit poslednu cislicu a vytvorit jednotny token zo zvysnych cislic
						pane1.setText(pane1.getText().substring(0, pane1.getText().length() - 1));
						// token name je o 1 mensi
						String name = symbols.get(symbols.size()-1).second().substring(0, symbols.get(symbols.size()-1).second().length()-1);
						symbols.remove(symbols.size()-1);
						pane2.setText(name);
						if ((panel.length() == 2) && panel.substring(0, 1).equals("-")){
							symbols.clear();
							symbols.add(Pair.createPair(TokenType.TOP, "$"));
							pane1.setText("");
							pane2.setText("");
						}
						else if (name.length() > 0 && name.substring(0, name.length()-1).equals(".")) {
							// posledna je bodka -> rozdelit na 2 tokeny (cislicu a bodku)
							symbols.remove(symbols.size()-1);
							symbols.add(Pair.createPair(TokenType.NUMBER, name.substring(0, name.length()-2)));
							symbols.add(Pair.createPair(TokenType.DOT, "."));
						}
						else if (name.length() > 0) {
							// posledna cislica vymazana -> nepridavat jej zvysok (ziaden nema)
							symbols.add(Pair.createPair(TokenType.NUMBER, name));
							tmp = false;
						}
					} // vymazanie cisla
					else {
						int size = symbols.get(symbols.size()-1).second().length(); // dlzka symbolu -> kolko znakov mame vymazat
						pane1.setText(pane1.getText().substring(0, pane1.getText().length() - size));
						symbols.remove(symbols.size()-1);
						tmp = true;
						pane2.setText(symbols.get(symbols.size()-1).second());
					} // vymazanie ineho tokenu
				}
			}
		};
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '.'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '.'
	 */
	public ActionListener makeDotActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() == TokenType.NUMBER && (! ContainsDot())) {
					symbols.add(Pair.createPair(TokenType.DOT, "."));
					if (!tmp) {pane2.setText("");}
					tmp = true;
					pane2.setText(pane2.getText() + ".");
					pane1.setText(pane1.getText() + ".");
				}
			}
		};
	}
	public boolean ContainsDot() {
		if (symbols.get(symbols.size()-1).second().contains(".") ){
			return true;
		}
		return false;
	}
	
	/**
	 * Metoda pro vytvorenie objektu reagujuceho na vstup znaku '=/ENTER'
	 *
	 * @return Vrati objekt reagujuci na vstup znaku '=/ENTER'
	 */
	public ActionListener makeEqualsActionListener() {
		return new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (symbols.get(symbols.size()-1).first() == TokenType.NUMBER || symbols.get(symbols.size()-1).first() == TokenType.FACTORIAL) {
					
					String result = "",newOperator = "",exp_sym = "$";
					int counter = 1,row = 0,col = 0;
					double tmp_result = 0.0,number1 = 0.0,number2 = 0.0;
					
					Operator operator = new Operator();
					Stack<String> st = new Stack<String>();
					Boolean repeat = true;
					
					st.push(exp_sym);
					while (repeat){

						switch(symbols.get(symbols.size() - counter).first()){
							
							case NUMBER://push cisla na zasobnik
								
								if ((symbols.size() <= 2)&&(symbols.get(symbols.size() - 2).second() == "$")){
									result = symbols.get(symbols.size() - counter).second();
									repeat = false;
									break;
								}
								
								newOperator = symbols.get(symbols.size() - counter - 1).second();
								row = tokenToInt(exp_sym);
								col = tokenToInt(newOperator);
								
								switch(PTable[row][col]){//push/reduckia podla precedencnej tabulky
								
									case SH://push na zasobnik
										st.push(symbols.get(symbols.size() - counter).second());
										exp_sym = newOperator;
										counter++;
										break;
										
									case RE://redukcia
										String op = st.pop();
										
										switch(op){//medzivypocet
											case "+":
												tmp_result = operator.add(Double.parseDouble(symbols.get(symbols.size() - counter).second()),Double.parseDouble(st.pop()));
												break;
											case "-":
												tmp_result = operator.sub(Double.parseDouble(symbols.get(symbols.size() - counter).second()),Double.parseDouble(st.pop()));
												break;
											case "*":
												tmp_result = operator.mul(Double.parseDouble(symbols.get(symbols.size() - counter).second()),Double.parseDouble(st.pop()));
												break;
											case "/":
												tmp_result = operator.div(Double.parseDouble(symbols.get(symbols.size() - counter).second()),Double.parseDouble(st.pop()));
												break;
											case "\u221A":
												tmp_result = operator.root(Double.parseDouble(st.pop()),Double.parseDouble(symbols.get(symbols.size() - counter).second()));
												break;
											case "^":
												tmp_result = operator.power(Double.parseDouble(symbols.get(symbols.size() - counter).second()),Double.parseDouble(st.pop()));
												break;
											case "%":
												tmp_result = operator.mod(Double.parseDouble(symbols.get(symbols.size() - counter).second()),Double.parseDouble(st.pop()));
												break;
											default:
												break;
										}
										exp_sym = newOperator;
										newOperator = st.peek();
										if (exp_sym == "$" && newOperator == "$"){
											result = Double.toString(tmp_result);
											st.pop();
											repeat = false;
											break;
										}
										st.push(Double.toString(tmp_result));//medzivypocet na zasobnik
										counter++;
										break;
									default:
										break;
								}
								break;
								
							case OPERATOR://vloznie operandu na zasobnik
								st.push(symbols.get(symbols.size() - counter).second());
								counter++;
								break;
								
							case TOP://spatny sucet z prava do lava
								number1 = Double.parseDouble(st.pop());
								newOperator = st.pop();//zmazanie operandu zo sazobnika
								number2 = Double.parseDouble(st.pop());
								switch (newOperator){//vypocet
								
									case "+":
										tmp_result = (operator.add(number1, number2));
										break;
									case "-":
										tmp_result = (operator.sub(number1, number2));
										break;
									case "*":
										tmp_result = (operator.mul(number1, number2));
										break;
									case "/":
										tmp_result = (operator.div(number1, number2));
										break;
									case "\u221A":
										tmp_result = (operator.root(number2,number1));
										break;
									case "^":
										tmp_result = (operator.power(number1, number2));
										break;
									case "%":
										tmp_result = (operator.mod(number1, number2));
										break;
									default:
										break;
								}
								newOperator = st.peek();
								if (newOperator == "$"){//posledny medzisucet na zasobniku
									repeat = false;
									result = Double.toString(tmp_result);
									break;
								}
								st.push(Double.toString(tmp_result));
								break;
								
							case FACTORIAL:
								counter++;
								tmp_result = operator.fact(Double.parseDouble(symbols.get(symbols.size() - counter).second()));
								st.push(Double.toString(tmp_result));
								counter++;
								if ((symbols.size() >= counter + 2)&&(symbols.get(symbols.size() - counter - 2).second() == "$")){
									exp_sym = symbols.get(symbols.size() - counter).second();
								}
								newOperator = symbols.get(symbols.size() - counter).second();
								if (exp_sym == "$" && newOperator == "$"){
									result = st.pop();
									st.pop();
									repeat = false;
									break;
								}
								break;
							default:
								break;
						}//switch Number/Operator/Top
					}// while vsetky lexemi
					tmp = true;
					double res = Double.parseDouble(result);
					int res_int = (int) Math.round(res);
					if (res == res_int){
						result = Integer.toString(res_int);
					}
					pane1.setText(result);
					symbols.clear();
					symbols.add(Pair.createPair(TokenType.TOP, "$"));
					symbols.add(Pair.createPair(TokenType.NUMBER, result));
					pane2.setText(result); //celkovy vysledok
				}
			}
		};
	}
	
	public Integer tokenToInt(String token){
		int num_token;
		switch (token){
		case "+":
		case "-":
			num_token = 0;
			break;
		case "*":
		case "/":
		case "%":	
			num_token = 1;
			break;
		case "\u221A":
		case "^":
			num_token = 2;
			break;
		case "!":
			num_token = 3;
			break;
		default:
			num_token = 6;
		}
		return num_token;
	}
}
