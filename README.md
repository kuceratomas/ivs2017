## **Prerekvizity** ##

* nainštalované balíčky ***java*** a ***javac*** (ver. 1.8)
* *(volitelné)* balíček ***ant*** pre manuálnu inštaláciu

---------
## Inštalácia ##

* pomocou priloženého inštalátoru
* manuálne pomocou kombinácie ***makefile*** a ***ant***
    * all (preloží projekt)
    * pack (zabalí projekt)
    * test (spustí testy matematickej knižnice)
    * doc (vygeneruje programovú dokumentáciu)
    * run (spustí program)
---------
## Odinštalácia ##

* Pri automatickej inštalácií: pomocou vytvoreného odinštalátoru
* Pre manuálnu inštaláciu:
    * make clean (zmaže všetky súbory vytvorené programom a jeho inštaláciou)
---------
## Použitie ##

* Kalkulačka reaguje aj na stlačenie kláves na klávesnici

---------
### Prostredi ###

Ubuntu 64bit

------
### Autori ###

Chvalabohu

- xkrama04 Adam   Kramár

- xkucer90 Tomáš  Kučera 

- xjakub31 Samuel Jakubík

-------
### Licence ###

Tento program je poskytovan pod licencí GNU GPL v3